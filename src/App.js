import { useEffect, useState } from "react";
import { BrowserRouter } from 'react-router-dom';
import { useAuth } from "./hooks/useAuth";
import { socket } from "./socket";
import './App.css';
import AnimatedRoutes from "./components/AnimatedRoutes";
import Navigation from './components/Navigation';
import Popup from 'reactjs-popup';
import imageChienChat from './assets/img/cohabitation-chien-chat.jpg';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCircleCheck, faHouse, faBell, faMessage, faCircleQuestion, faUser, faPaw, faPlus, faMinus, faCamera, faCheck, faMapLocationDot, faBullhorn } from '@fortawesome/free-solid-svg-icons';
// import { faBell } from '@fortawesome/free-regular-svg-icons';

library.add(fab, faCircleCheck, faHouse, faCircleQuestion, faBell, faMessage, faUser, faPaw, faPlus, faMinus, faCamera, faCheck, faMapLocationDot, faBell, faBullhorn);

function App() {
    const { authenticate } = useAuth();
    const [isConnected, setIsConnected] = useState(socket.connected);
    const [open, setOpen] = useState(false);
    const [newAnnounce, setNewAnnounce] = useState({ title: '', description: '', adresse: ''});

    const closeModal = () => {
        setOpen(false);
    };

    useEffect(() => {
        authenticate();

        function onConnect() {
            setIsConnected(true);
        }
        function onDisconnect() {
            setIsConnected(false);
        }

        socket.on('connect', onConnect);
        socket.on('disconnect', onDisconnect);

        socket.connect();

        socket.on('newAnnounce', (announce) => {
            setNewAnnounce(announce.announce.alert);
            setOpen(true);
        });

        return () => {
            socket.off('connect', onConnect);
            socket.off('disconnect', onDisconnect);
        };
    }, []);

    return (
        <BrowserRouter>
        <div className="App">
            <Popup open={open} onClose={closeModal}>
            <div id="default-modal" aria-hidden="true" class="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 flex justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
                <div class="relative p-4 w-full max-w-2xl max-h-full">
                    <div class="relative bg-white rounded-lg shadow-2xl">
                        <div class="flex items-center justify-between p-4 md:p-5 border-b rounded-t">
                            <h3 class="text-xl font-semibold text-gray-900">
                                Nouvelle annonce
                            </h3>
                            <button onClick={closeModal} type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center" data-modal-hide="default-modal">
                                <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                                </svg>
                                <span class="sr-only">Fermer</span>
                            </button>
                        </div>
                        <div className="flex">
                            <div class="p-4 md:p-5 w-1/2 space-y-4">
                                <p class="text-base leading-relaxed text-gray-500">
                                    { newAnnounce.title }
                                </p>
                                <p class="text-base leading-relaxed text-gray-500">
                                    { newAnnounce.description }
                                </p>
                                <p class="text-base leading-relaxed text-gray-500">
                                    Adresse : { newAnnounce.adresse }
                                </p>
                            </div>
                            <img className="w-1/2 p-2 object-cover" src={imageChienChat} alt="Neil animal"/>
                        </div>
                        <div class="flex items-center p-4 md:p-5 border-t border-gray-200 rounded-b">
                            <button onClick={closeModal} data-modal-hide="default-modal" type="button" class="text-white bg-secondary hover:bg-secondary-light focus:ring-4 focus:outline-none focus:ring-secondary font-medium rounded-lg text-sm px-5 py-2.5 text-center">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            </Popup>
            <AnimatedRoutes />
            <Navigation />
        </div>
        </BrowserRouter>
    );
}

export default App;
