import { create } from "zustand";
import { combine, persist } from "zustand/middleware";

export const useAccountStore = create(
    persist(
        combine(
            {
                account: undefined,
            },
            (set) => ({
                setAccount: async (account) => set({ account }),
            })
        ),
        {
            name: "account",
        }
    )
);