import { useAccountStore } from "../store";
import axios from 'axios';

export function useAuth() {
    const AuthStatus = {
        Unknown: 0,
        Authenticated: 1,
        Guest: 2,
    };
    const { account, setAccount } = useAccountStore();
    let status;
    switch (account) {
        case null:
            status = AuthStatus.Guest;
            break;
        case undefined:
            status = AuthStatus.Unknown;
            break;
        default:
            status = AuthStatus.Authenticated;
            break;
    }

    const authenticate = () => {
        if (account !== undefined && account !== null) {
            let token = account;
            if (account.token !== undefined) {
                token = account.token;
            }
            axios.post('http://localhost:3001/api/users/checkToken', { token })
            .then(res => {
                if (res.data.error !== undefined) {
                    logout();
                    return;
                }
                setAccount({token: token, user :res.data});
            });
        } else {
            setAccount(null);
        }
    };
    
    const login = async (email, password) => {
        let result = false
        const data = { email: email, password: password };
        await axios.post(`http://localhost:3001/login`, { ...data })
        .then(async res => {
            if (res.data.error !== undefined) {
                return false;
            }
            await setAccount(res.data);
            result = true;
        });
        return result;
    };

    const logout = () => {
        setAccount(null);
    };

    return {
        status,
        authenticate,
        login,
        logout,
    };
}