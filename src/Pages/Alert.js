import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useAccountStore } from "../store";
import MapComponent from "../components/Map";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const Alert = () => {
    let { id } = useParams();
    const navigate = useNavigate();
    const { account } = useAccountStore();
    const [isPageLoaded, setIsPageLoaded] = useState(false);
    const [alert, setAlert] = useState([]);
    const [animal, setAnimal] = useState([]);
    const [userResponseStatut, setUserResponseStatut] = useState(false);

    useEffect(() => {
        axios.get('http://localhost:3001/api/alerts/' + id)
            .then((res) => {
                setAlert(res.data);
            })
            .catch((err) => {
                console.log(err);
            });

        axios.get('http://localhost:3001/api/animals/' + id)
            .then((res) => {
                setAnimal(res.data);
            })
            .catch((err) => {
                console.log(err);
            });

        axios.get('http://localhost:3001/api/alerts-answers/user/' + account.user._id)
            .then((res) => {
                res.data.forEach(element => {
                    if (element.alertId === id && element.userId === account.user._id) {
                        setUserResponseStatut(true);                        
                    } else {
                        setUserResponseStatut(false);
                    }
                });
            })
            .catch((err) => {
                console.log(err);
            });

        // Simuler un chargement de page
        setTimeout(() => {
            setIsPageLoaded(true);
        }, 1000);
    }, []);

    const deleteAlerts = (e) => {
        axios.delete('http://localhost:3001/api/alerts/' + id)
            .then((res) => {
                navigate('/');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="alerts w-full h-full">
            <h1 className='text-4xl font-bold text-primary'>{ alert.title }</h1>
            <div className="flex w-full">
                <div className="bg-primary-light h-[3px] w-full"></div>
            </div>
            <div className="flex items-center gap-x-12 w-full">
                <div className="w-1/3">
                    {isPageLoaded ? (
                        <div className="relative w-full h-48">
                            <MapComponent animals={animal} announces={alert} />
                        </div>
                    ) : (
                        <div>Carte en cours de chargement...</div>
                    )}
                </div>
                <div className="w-2/3">
                    <img className="rounded-lg" src={imageChienChat} alt="Neil animal"/>
                </div>
            </div>
            <form className="flex flex-col items-start w-full gap-1">
                <label className="font-bold text-primary mt-2 uppercase">TYPE</label>
                <span 
                    className={`font-bold text-primary ${alert.type === 'Perdu' ? 'block py-1 px-2 bg-red-500 text-white rounded-full' : ''} ${alert.type === 'Trouvé' ? 'block py-1 px-2 bg-green-500 text-white rounded-full' : ''}`}
                >
                    { alert.type }
                </span>
                <label className="font-bold text-primary mt-2 uppercase">DATE</label>
                <span className="font-bold text-primary">{ alert.date }</span>
                <label className="font-bold text-primary mt-2 uppercase">ADRESSE</label>
                <span className="font-bold text-primary">{ alert.adresse }</span>
                <label className="font-bold text-primary mt-2">DESCRIPTION</label>
                <p className="font-bold text-primary text-left">{ alert.description }</p>
            </form>
            {
                account.user._id !== alert.userId ? (
                    !userResponseStatut ? (
                        <Link
                            to={`/answeradd/${alert._id}`}
                            className='btn text-green-500 border border-green-500 font-bold rounded-xl px-8 py-2 cursor-pointer uppercase'
                        >
                            Je connais cet animal / C'est mon animal
                        </Link>
                    ) : (
                        <span 
                            className='btn text-green-500 border border-green-500 font-bold rounded-xl px-8 py-2 uppercase'
                        >
                            <FontAwesomeIcon icon="fa-solid fa-circle-check" className="pr-4" />
                            Vous avez déjà répondu à l'alerte
                        </span>
                    )
                ) : null
            }
            <div className="flex justify-between gap-x-4">
                <Link 
                    to='/alerts' 
                    className='btn bg-primary text-white font-bold rounded-xl px-8 py-2 cursor-pointer uppercase'
                >
                    Voir les alertes
                </Link>
                {
                    account.user._id === alert.userId && (
                        <span 
                            onClick={deleteAlerts}
                            className='btn bg-secondary text-white font-bold rounded-xl px-8 py-2 cursor-pointer uppercase' 
                        >
                            Supprimer l'alerte
                        </span>
                    )
                }
            </div>
        </div>
    );
}

export default Alert;