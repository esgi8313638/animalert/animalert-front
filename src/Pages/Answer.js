import { Link, useParams } from "react-router-dom";
import { useEffect, useState, Fragment } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useAccountStore } from "../store";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PopupValidation from "../components/popup/PopupValidation";

import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const Answer = () => {
    let { id } = useParams();
    const navigate = useNavigate();
    const { account } = useAccountStore();
    const [answer, setAnswer] = useState({});
    const [animals, setAnimals] = useState([]);
    const [animalAnswer, setAnimalAnswer] = useState({});
    let [isPopupOpen, setPopupOpen] = useState(false);

    let [popupData, setPopupData] = useState({
        title: '',
        description: '',
        color: ''
    });

    useEffect(() => {
        axios.get('http://localhost:3001/api/alerts-answers/' + id)
            .then((res) => {
                setAnswer(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        axios.get('http://localhost:3001/api/animals/user/' + account.user._id)
            .then((res) => {
                setAnimals(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    },[]);
    
    useEffect(() => {
        if (answer?.animalId) { // On vérifie si answer est défini et que animalId est défini
            axios.get('http://localhost:3001/api/animals/' + answer.animalId)
                .then((res) => {
                    setAnimalAnswer(res.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }, [answer]);

    const validateResponse = (e) => {
        animals.forEach(element => {
            if (element._id === answer.animalId) {
                setPopupData({
                    title: 'Votre réponse est validée',
                    description: 'Maintenant que la réponse est validée, l\'annonce sera supprimée dans 5 secondes et vous serez redirigé vers la liste des alertes.',
                    color: 'green-500'
                });
                setPopupOpen(true);
        
                setTimeout(() => {
                    axios.delete('http://localhost:3001/api/alerts/' + answer.alertId)
                        .catch((err) => {
                            console.log(err);
                        });

                    axios.delete('http://localhost:3001/api/alerts-answers/' + answer._id)
                        .then((res) => {
                            navigate('/alerts');
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                }, 5000);
            } else {
                setPopupData({
                    title: 'Erreur',
                    description: 'Vous ne pouvez pas valider cette réponse car l\'animal concerné ne vous appartient pas.',
                    color: 'red-500'
                });
                console.log(popupData);
                setPopupOpen(true);
            }
        });
    };
    
    const deleteAnswer = (e) => {
        axios.delete('http://localhost:3001/api/alerts-answers/' + id)
            .then((res) => {
                navigate('/alerts');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="alerts w-full h-full">
            <h1 className='text-4xl font-bold text-primary'>{ answer.title }</h1>
            <div className="flex w-full">
                <div className="bg-primary-light h-[3px] w-full"></div>
            </div>
            <div className="flex items-center gap-x-12 w-full">
                <div className='flex flex-col items-center animal'>
                    <label className="font-bold text-primary mt-2 uppercase">Animal concerné</label>
                    <img className="w-20 h-20 rounded-full object-cover border-[2px] border-primary pointer-events-none" src={imageChienChat} alt="Neil animal"/>
                    <p className='text-primary font-semibold text-lg uppercase pointer-events-none'>{ animalAnswer.name }</p>
                </div>
                <div className="w-2/3">
                    <img className="rounded-lg" src={imageChienChat} alt="Neil animal"/>
                </div>
            </div>
            <div className="flex flex-col items-start gap-1">
                <label className="font-bold text-primary mt-2 uppercase">TYPE</label>
                <span 
                    className='block py-2 px-4 bg-green-500 text-white rounded-full font-bold'
                >
                    { answer.type }
                </span>
            </div>
            <form className="flex flex-col items-start w-full gap-1">
                <label className="font-bold text-primary mt-2 uppercase">DATE</label>
                <span className="font-bold text-primary">{ answer.date }</span>
                <label className="font-bold text-primary mt-2 uppercase">ADRESSE</label>
                <span className="font-bold text-primary">{ answer.adresse }</span>
                <label className="font-bold text-primary mt-2">DESCRIPTION</label>
                <p className="font-bold text-primary text-left">{ answer.description }</p>
            </form>
            {
                answer.type === 'C\'est mon animal' && (
                    <div className="flex flex-col gap-y-2">
                        <h2 className='text-2xl font-bold text-primary text-left'>Procéder à la validation de la réponse</h2>
                        <span 
                            onClick={validateResponse}
                            className='btn border border-green-500 text-green-500 font-bold rounded-xl px-8 py-2 cursor-pointer uppercase' 
                        >
                            Valider la propriété de l'animal
                        </span>
                    </div>
                )
            }
            <div className="flex justify-between gap-x-4 pt-8">
                <Link 
                    to='/alerts' 
                    className='btn bg-primary text-white font-bold rounded-xl px-8 py-2 cursor-pointer uppercase'
                >
                    Voir les autres alertes
                </Link>
                {
                    account.user._id === answer.userId && (
                        <span 
                            onClick={deleteAnswer}
                            className='btn bg-secondary text-white font-bold rounded-xl px-8 py-2 cursor-pointer uppercase' 
                        >
                            Supprimer la réponse
                        </span>
                    )
                }
            </div>
            <PopupValidation 
                open={isPopupOpen} 
                onClose={() => setPopupOpen(false)}
                title={popupData.title}
                description={popupData.description}
                color={popupData.color}
            />
        </div>
    );
}

export default Answer;