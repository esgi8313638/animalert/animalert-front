import { Link, useNavigate } from "react-router-dom";
import { motion } from 'framer-motion';
import axios from "axios";
import { useAccountStore } from "../store";
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const Alerts = () => {
    const { account } = useAccountStore();
    const [alerts, setAlerts] = useState([]);
    const [filteredAlerts, setFilteredAlerts] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3001/api/alerts/')
            .then((res) => {
                setAlerts(res.data);
                setFilteredAlerts(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const selectTag = (e) => {
        const tag = e.target.dataset.tag;
        let newFilteredAlerts;
    
        if (tag === 'all') {
            newFilteredAlerts = alerts;
        } else {
            newFilteredAlerts = alerts.filter(alert => alert.type === tag);
        }
    
        const tags = document.querySelectorAll('.tag-list > div');
        tags.forEach(tag => {
            tag.classList.remove('border-secondary');
            tag.classList.add('border-white');
            tag.children[0].classList.remove('text-secondary');
            tag.children[0].classList.add('text-primary');
        });
    
        e.target.classList.remove('border-white');
        e.target.classList.add('border-secondary');
        e.target.children[0].classList.remove('text-primary');
        e.target.children[0].classList.add('text-secondary');

        setFilteredAlerts(newFilteredAlerts);
    };

    return (
        <motion.div
            className="alerts w-full h-full absolute top-0 left-0 p-6"
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <h1 className='text-4xl font-bold text-primary'>Les annonces / alertes</h1>
            <p className="text-primary text-lg font-semibold text-left">Ici, vous pouvez voir l'ensemble des alertes crées sur l'application. Vous pouvez les filtrer pour mieux correspondre à votre recherche.</p>
            <div className="flex flex-col">
                <h2 className='w-fit text-xl font-bold text-primary uppercase'>Les filtres</h2>
                <div style={{ width: 'fit-content' }} className='tag-list flex justify-start items-center gap-2 pt-1'>
                    <div onClick={selectTag} data-tag="all" className='flex justify-center border-[2px] border-white items-center w-fit px-4 py-2 bg-white rounded-full cursor-pointer select-none'>
                        <span className='pointer-events-none font-semibold text-primary'>Tous</span>
                    </div>
                    <div onClick={selectTag} data-tag="Perdu" className='flex justify-center border-[2px] border-white items-center w-fit px-4 py-2 bg-white rounded-full cursor-pointer select-none'>
                        <span className='pointer-events-none font-semibold text-primary'>Animaux perdus</span>
                    </div>
                    <div onClick={selectTag} data-tag="Trouvé" className='flex justify-center border-[2px] border-white items-center w-fit px-4 py-2 bg-white rounded-full cursor-pointer select-none'>
                        <span className='pointer-events-none font-semibold text-primary'>Animaux trouvés / aperçus</span>
                    </div>
                </div>
            </div>
            <div className="w-full">
                <div className="flow-root">
                    <ul role="list" className="flex flex-wrap gap-2">
                        {filteredAlerts.length > 0 ? (
                            filteredAlerts.map((alert, index) => (
                                <li key={index} className="max-w-60 border border-secondary rounded-lg">
                                    <img className="rounded-t-lg" src={imageChienChat} alt="" />
                                    <div className="p-3">
                                        <h5 className="mb-2 text-xl font-bold tracking-tight text-primary">{alert.title}</h5>
                                        <p className="mb-5 font-normal text-primary">{alert.description}</p>
                                        <Link
                                            to={`/alert/${alert._id}`}
                                            className="btn text-sm bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full uppercase"
                                        >
                                            Voir l'annonce
                                        </Link>
                                    </div>
                                </li>
                            ))
                        ) : (
                            <p className='text-primary text-sm font-semibold w-full text-left'>Il n'y a pas d'annonces</p>
                        )}
                    </ul>
                </div>
            </div>
            <Link
                to='/announceadd'
                className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full uppercase'
            >
                Créer une annonce
            </Link>
        </motion.div>
    );
};

export default Alerts;