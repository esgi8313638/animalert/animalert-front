import { motion } from 'framer-motion';
import { useAccountStore } from "../store";
import { Link } from 'react-router-dom';
import { useRef, useState, useEffect } from 'react';
import { useDraggable } from "react-use-draggable-scroll";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MapComponent from '../components/Map';
import SearchFilters from '../components/SearchFilters';
import axios from "axios";
import { useNavigate } from "react-router-dom";

import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const Home = () => {
    const navigate = useNavigate();
    const ref = useRef();
    const { events } = useDraggable(ref);
    const { account } = useAccountStore();
    const [animals, setAnimals] = useState([]);
    const [announces, setAnnounces] = useState([]);
    const [answers, setAnswers] = useState([]);

    const refreshMap = (e) => {
        e.preventDefault();
        axios.get('http://localhost:3001/api/alerts')
            .then((res) => {
                setAnnounces(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const showAnimal = (e) => {
        navigate('/animal/' + e.target.dataset.id);
    };

    useEffect(() => {
        if (account === undefined) {
            return;
        }
        axios.get('http://localhost:3001/api/animals/user/' + account.user._id)
            .then((res) => {
                setAnimals(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        axios.get('http://localhost:3001/api/alerts')
            .then((res) => {
                setAnnounces(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        axios.get('http://localhost:3001/api/alerts-answers/user/' + account.user._id)
            .then((res) => {
                setAnswers(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <motion.div 
            style={{justifyContent: 'space-between'}}
            className='home !items-center w-full h-full absolute top-0 left-0 flex flex-col'
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <div className='absolute w-full h-4/6'>
                <MapComponent animals={animals} announces={announces} />
            </div>
            <SearchFilters />
            <div className='w-full h-[68%] flex flex-col gap-3 justify-between items-center z-20'>
                <button onClick={refreshMap} className='bg-primary text-white flex gap-3 items-center px-10 py-2 rounded-full font-bold font-clash-display uppercase'>Actualiser la carte</button>
                <div style={{backgroundColor: '#FDF8EC'}} className='scroll-hidden w-full h-full flex flex-col gap-4 justify-start items-start px-10 bg-white rounded-t-[50px] overflow-y-auto overflow-x-hidden pb-28' {...events} ref={ref}>
                    <div className='w-1/6 h-1 self-center mt-4 bg-primary rounded-full'></div>
                    <h2 className='text-primary text-3xl font-semibold mt-2'>Vos animaux</h2>
                    <div className='flex gap-6 cursor-pointer'>
                        {
                            animals.map((animal, index) => {
                                return (
                                    <div 
                                        onClick={showAnimal} 
                                        key={index} 
                                        data-id={animal._id} 
                                        className='flex flex-col items-center'
                                    >
                                        <img className="pointer-events-none w-36 h-28 rounded-xl object-cover" src={imageChienChat} alt="Neil animal"/>
                                        <p className='pointer-events-none text-primary font-semibold text-sm w-4/5'>{ animal.name }</p>
                                    </div>
                                )
                            })
                        }
                        {
                            animals.length === 0 ? <p className='text-primary text-sm font-semibold'>Vous n'avez pas encore d'animaux.</p> : null
                        }
                    </div>
                    <h2 className='text-primary text-3xl font-semibold'>Les dernières annonces</h2>
                    <div className='relative flex gap-6'>
                        {announces.length > 0 ? (
                            announces.slice(0, 5).map((announce, index) => {
                                return (
                                    <Link
                                        to={'/alert/' + announce._id}
                                        key={index} 
                                        className='flex flex-col items-center gap-5 w-56 p-6 bg-white rounded-lg cursor-pointer'
                                    >
                                        <div className='flex justify-between items-center w-full'>
                                            <img className="w-16 h-16 rounded-full object-cover border-[1px] border-primary" src={imageChienChat} alt="Neil animal"/>
                                            <div className='flex flex-col items-end gap-y-2'>
                                                <p className='text-primary font-semibold text-sm text-right'>{ announce.title }</p>
                                                <span 
                                                    className={`w-fit py-1 px-2 rounded-full text-white text-sm text-right ${announce.type === 'Perdu' ? 'bg-red-500' : 'bg-green-500'}`}
                                                >
                                                    { announce.type }
                                                </span>
                                            </div>
                                        </div>
                                        <div className='flex flex-col w-full justify-start text-left'>
                                            <p className='text-primary font-semibold text-sm w-fit'>{ announce.adresse }</p>
                                            <p className='pt-2 text-primary text-sm w-fit'>{ announce.description }</p>
                                        </div>
                                    </Link>
                                )
                            })
                        ) : (
                            <p className="text-primary">Aucune annonce disponible</p>
                        )}
                    </div>
                    <h2 className='text-primary text-3xl font-semibold'>Vos dernières réponses.</h2>
                    <div className='relative flex gap-6'>
                        {answers.length > 0 ? (
                            answers.slice(0, 3).map((answer, index) => {
                                return (
                                    <Link
                                        to={'/answer/' + answer._id}
                                        key={index} 
                                        className='flex flex-col items-center gap-5 w-56 p-6 bg-white rounded-lg cursor-pointer'
                                    >
                                        <div className='flex flex-col gap-y-2 items-center w-full'>
                                            <img className="w-16 h-16 rounded-full object-cover border-[1px] border-primary" src={imageChienChat} alt="Neil animal"/>
                                            <p className='text-primary font-semibold text-sm text-center'>{ answer.title }</p>
                                            <span 
                                                className={`w-fit py-1 px-2 rounded-full text-white text-sm text-right ${answer.type === 'Perdu' ? 'bg-red-500' : 'bg-green-500'}`}
                                            >
                                                { answer.type }
                                            </span>
                                        </div>
                                    </Link>
                                )
                            })
                        ) : (
                            <p className="text-primary">Vous n'avez proposé aucunes réponses.</p>
                        )}
                    </div>
                </div>
            </div>
        </motion.div>
    );
}

export default Home;