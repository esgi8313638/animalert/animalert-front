import { Link, useNavigate } from "react-router-dom";
import { motion } from 'framer-motion';
import axios from "axios";
import { useAccountStore } from "../store";
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const Answers = () => {
    const { account } = useAccountStore();
    const [answers, setAnswers] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3001/api/alerts-answers/user/' + account.user._id)
            .then((res) => {
                setAnswers(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <motion.div
            className="answers w-full h-full absolute top-0 left-0 p-6"
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <h1 className='text-4xl font-bold text-primary'>Vos réponses</h1>
            <p className="text-primary text-lg font-semibold text-left">Ici, vous pouvez voir l'ensemble des réponses que vous avez crée ou qui concerne vos différents animaux.</p>
            <div className="w-full">
                <div className="flow-root">
                    <ul role="list" className="flex flex-wrap gap-2">
                        {answers.length > 0 ? (
                            answers.map((answer, index) => (
                                <li key={index} className="max-w-60 border border-secondary rounded-lg">
                                    <img className="rounded-t-lg" src={imageChienChat} alt="" />
                                    <div className="p-5">
                                        <span 
                                            className='block text-sm py-1 px-2 mb-2 bg-green-500 text-white rounded-full font-bold'
                                        >
                                            { answer.type }
                                        </span>
                                        <h5 className="mb-4 text-xl font-bold tracking-tight text-primary">{answer.title}</h5>
                                        <Link
                                            to={`/answer/${answer._id}`}
                                            className="btn text-sm bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full uppercase"
                                        >
                                            Voir la réponse
                                        </Link>
                                    </div>
                                </li>
                            ))
                        ) : (
                            <p className='text-primary text-sm font-semibold w-full text-left'>Il n'y a pas de réponses</p>
                        )}
                    </ul>
                </div>
            </div>
            <Link
                to='/alerts'
                className='btn bg-primary text-white font-bold rounded-xl px-4 py-2.5 w-full uppercase'
            >
                Retour aux annonces
            </Link>
        </motion.div>
    );
};

export default Answers;