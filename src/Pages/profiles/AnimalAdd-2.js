import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from "react";
import { useCookies } from 'react-cookie';

const AnimalAdd2 = () => {
    const [weight, setWeight] = useState(4.2);
    const [animalData, setAnimalData] = useState({ name: '', species: '', breed: '', puceID: '', tattoo: '', color: '', fur: '', height: 0, weight: 0, description: '', pictures: '' });
    const [cookies, setCookie] = useCookies(['newAnimal']);

    useEffect(() => {
        setAnimalData(cookies.newAnimal);
    }, []);

    const handleInputChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        setAnimalData({ ...animalData, [name]: value })
        setCookie('newAnimal', animalData );
    };

    const incrementWeight = () => {
        if (weight >= 30) return;
        setWeight((prevWeight) => parseFloat((prevWeight + 0.1).toFixed(1)));
        setAnimalData({ ...animalData, weight: weight });
    }

    const decrementWeight = () => {
        if (weight <= 0.1) return;
        setWeight((prevWeight) => parseFloat((prevWeight - 0.1).toFixed(1)));
        setAnimalData({ ...animalData, weight: weight });
    }

    return (
        <div className="animals w-full h-full">
            <div className="logo w-24 h-24 rounded-full bg-primary"></div>
            <h1 className='text-2xl font-bold text-primary'>Nouvel animal</h1>
            <div className="flex w-full">
                <div className="bg-primary h-[3px] w-2/4"></div>
                <div className="bg-primary-light h-[3px] w-2/4"></div>
            </div>
            <div className="flex flex-col items-center w-full gap-1">
                <div className="flex flex-col gap-2">
                    <label className="font-bold text-primary mt-2 uppercase text-2xl">Son poids</label>
                    <div className="px-10 py-4 border-[1px] border-primary rounded-3xl">
                        <span className="font-clash-display text-secondary font-black text-5xl">{weight}</span>
                        <p className="text-primary font-semibold text-sm">Kilogrammes</p>
                    </div>
                    <div className="w-full flex justify-between items-center">
                        <button onClick={incrementWeight} className="btn bg-transparent text-primary font-bold rounded-xl px-4 py-2 border-[1px] border-primary transition hover:bg-primary hover:text-white">
                            <FontAwesomeIcon icon="fa-solid fa-plus" />
                        </button>
                        <button onClick={decrementWeight} className="btn bg-transparent text-primary font-bold rounded-xl px-4 py-2 border-[1px] border-primary transition hover:bg-primary hover:text-white">
                            <FontAwesomeIcon icon="fa-solid fa-minus" />
                        </button>
                    </div>
                </div>
                <label className="font-bold text-primary uppercase text-2xl mt-2">Description</label>
                <textarea value={animalData.description} onChange={handleInputChange} name="description" rows="6" className='w-full rounded-lg border-[1px] border-primary bg-transparent py-3 px-4 placeholder-primary resize-none' placeholder="Description de l'animal..."></textarea>

                <Link to='/animaladd-3' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-2'>CONTINUER</Link>
            </div>
            <Link to='/animals' className='btn bg-primary text-white font-bold rounded-xl px-10 py-2'>ANNULER</Link>
        </div>
    );
}

export default AnimalAdd2;