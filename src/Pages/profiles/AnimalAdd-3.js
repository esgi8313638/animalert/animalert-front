import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from "react";
import { useCookies } from 'react-cookie';

const AnimalAdd3 = () => {
    const [animalData, setAnimalData] = useState({ name: '', species: '', breed: '', puceID: '', tattoo: '', color: '', fur: '', height: 0, weight: 0, description: '', pictures: '' });
    const [cookies, setCookie] = useCookies(['newAnimal']);

    useEffect(() => {
        setAnimalData(cookies.newAnimal);
    }, []);

    const handleInputChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                const base64String = reader.result;
                animalData.pictures = base64String;
                setCookie('newAnimal', animalData );
            };
            reader.readAsDataURL(file);
        }
    };

    return (
        <div className="animals w-full h-full">
            <div className="logo w-24 h-24 rounded-full bg-primary"></div>
            <h1 className='text-2xl font-bold text-primary'>Nouvel animal</h1>
            <div className="flex w-full">
                <div className="bg-primary h-[3px] w-3/4"></div>
                <div className="bg-primary-light h-[3px] w-1/4"></div>
            </div>
            <div className="flex flex-col items-center w-full gap-1">
                <label htmlFor="photo" className="py-12 px-24 border-[1px] border-primary rounded-2xl mt-16">
                    <FontAwesomeIcon icon="fa-solid fa-camera" className="text-3xl" />
                </label>
                <input onChange={handleInputChange} type="file" id="photo" name="photo" accept="image/png, image/jpeg, image/webp, image/svg" className="hidden" />
                <label className="font-bold text-primary uppercase text-2xl mt-2">Photo de l'animal</label>
                <p className="font-semibold text-primary">Choisissez une photo de profil (5Mo maximum).</p>
                <p className="font-semibold text-primary text-xs">Format accepté : JPEG, PNG, SVG et WEBP.</p>
                <Link to='/animaladd-4' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-12'>CONTINUER</Link>
            </div>
            <Link to='/animals' className='btn bg-primary text-white font-bold rounded-xl px-10 py-2'>ANNULER</Link>
        </div>
    );
}

export default AnimalAdd3;