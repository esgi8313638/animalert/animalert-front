import { Link } from "react-router-dom";
import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useAccountStore } from "../../store";

const AnimalAdd1 = () => {
    const { account } = useAccountStore();
    const [animalData, setAnimalData] = useState({ userId: account.user._id, name: '', species: '', breed: '', puceID: '', tattoo: '', color: '', fur: '', height: 0, weight: 0, description: '', pictures: '' });
    const [cookies, setCookie] = useCookies(['newAnimal']);

    const handleInputChange = (e) => {
        const target = e.target;
        let value = target.value;
        const name = target.name;
        if (name === 'height') {
            value = parseFloat(value);
        }
        setAnimalData({ ...animalData, [name]: value })
        setCookie('newAnimal', animalData );
    };

    useEffect(() => {
        setCookie('newAnimal', animalData );
    }, []);

    return (
        <div className="animals w-full h-full">
            <div className="logo w-24 h-24 rounded-full bg-primary"></div>
            <h1 className='text-2xl font-bold text-primary'>Nouvel animal</h1>
            <div className="flex w-full">
                <div className="bg-primary h-[3px] w-1/4"></div>
                <div className="bg-primary-light h-[3px] w-3/4"></div>
            </div>
            <form className="flex flex-col items-start w-full gap-1">
                <label className="font-bold text-primary mt-2 uppercase">Nom</label>
                <input value={animalData.name} onChange={handleInputChange} name="name" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Son nom"/>
                <div className="flex w-full">
                    <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                        <label className="font-bold text-primary mt-2 uppercase">Espèce</label>
                        <input value={animalData.species} onChange={handleInputChange} name="species" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Son espèce"/>    
                    </div>
                    <div className="flex flex-col gap-1 w-1/2 items-start">
                        <label className="font-bold text-primary mt-2 uppercase">RACE</label>
                        <input value={animalData.breed} onChange={handleInputChange} name="breed" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Sa race"/>
                    </div>
                </div>
                <label className="font-bold text-primary mt-2 uppercase">Numéro de puce ou tatouage</label>
                <input value={animalData.puceID} onChange={handleInputChange} name="puceID" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Son numéro de puce ou de tatouage"/>
                <label className="font-bold text-primary mt-2">COULEUR</label>
                <input value={animalData.color} onChange={handleInputChange} name="color" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Sa couleur"/>
                <label className="font-bold text-primary mt-2 uppercase">TYPE DE POILS</label>
                <input value={animalData.fur} onChange={handleInputChange} name="fur" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Son type de poils"/>
                <label className="font-bold text-primary mt-2">TAILLE</label>
                <input value={animalData.height} onChange={handleInputChange} name="height" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Sa taille"/>
                <Link to='/animaladd-2' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-2'>CONTINUER</Link>
            </form>
            <Link to='/animals' className='btn bg-primary text-white font-bold rounded-xl px-10 py-2'>ANNULER</Link>
        </div>
    );
}

export default AnimalAdd1;