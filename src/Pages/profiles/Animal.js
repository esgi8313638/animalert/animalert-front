import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Animal = () => {
    let { id } = useParams();
    const navigate = useNavigate();
    const [animal, setAnimal] = useState({});

    useEffect(() => {
        axios.get('http://localhost:3001/api/animals/' + id)
            .then((res) => {
                setAnimal(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const deleteAnimal = (e) => {
        axios.delete('http://localhost:3001/api/animals/' + id)
            .then((res) => {
                navigate('/animals');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="animals w-full h-full">
            <h1 className='text-4xl font-bold text-primary'>{ animal.name }</h1>
            <div className="flex w-full">
                <div className="bg-primary-light h-[3px] w-full"></div>
            </div>
            <form className="flex flex-col items-start w-full gap-1">
                <div className="flex w-full">
                    <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                        <label className="font-bold text-primary mt-2 uppercase">Espèce</label>
                        <span className="font-bold text-primary">{ animal.species }</span>
                    </div>
                    <div className="flex flex-col gap-1 w-1/2 items-start">
                        <label className="font-bold text-primary mt-2 uppercase">RACE</label>
                        <span className="font-bold text-primary">{ animal.breed }</span>
                    </div>
                </div>
                <label className="font-bold text-primary mt-2 uppercase">Numéro de puce ou tatouage</label>
                <span className="font-bold text-primary">{ animal.puceID }</span>
                <label className="font-bold text-primary mt-2">COULEUR</label>
                <span className="font-bold text-primary">{ animal.color }</span>
                <label className="font-bold text-primary mt-2 uppercase">TYPE DE POILS</label>
                <span className="font-bold text-primary">{ animal.fur }</span>
                <label className="font-bold text-primary mt-2">TAILLE</label>
                <span className="font-bold text-primary">{ animal.height }</span>
                <label className="font-bold text-primary mt-2">POIDS</label>
                <span className="font-bold text-primary">{ animal.weight }</span>
                <label className="font-bold text-primary mt-2">DESCRIPTION</label>
                <span className="font-bold text-primary">{ animal.description }</span>
            </form>
            <div className="flex justify-between gap-x-4">
                <Link to='/animals' className='btn bg-primary text-white font-bold rounded-xl px-10 py-2 uppercase'>Voir les animaux</Link>
                <span 
                    onClick={deleteAnimal}
                    className='btn bg-secondary text-white font-bold rounded-xl px-10 py-2 cursor-pointer uppercase' 
                >
                    Supprimer l'animal
                </span>
            </div>
        </div>
    );
}

export default Animal;