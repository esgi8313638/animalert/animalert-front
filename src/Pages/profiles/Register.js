import { useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import axios from 'axios';

const Register = () => {
    const [registerData, setRegisterData] = useState({ firstName: '', name: '', surname: '', email: '', telNumber: '', password: '', address: '', localization: false, pictures: 0});
    const navigate = useNavigate();
    const { login } = useAuth();

    const register = async (e) => {
        e.preventDefault();
        if (registerData.name !== '' && registerData.firstname !== '' && registerData.surname !== '' && registerData.email !== '' && registerData.telNumber !== '' && registerData.password !== '') {
            axios.post(`http://localhost:3001/api/users`, { ...registerData })
            .then(async res => {
                const result = await login(
                    res.data.user.email,
                    res.data.user.password
                );
                if (result) {
                    navigate('/');
                }
            });
        }
    };

    const handleInputChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        setRegisterData({ ...registerData, [name]: value })
    };

    return (
        <div className="register w-full h-full">
            <div className="bg-primary-light rounded-xl h-[34px] flex">
                <Link to='/login' className="btn text-white text-md font-semibold px-4 h-full flex items-center">SE CONNECTER</Link>
                <Link to='/register' className="btn bg-primary text-white text-md font-semibold rounded-xl px-4 h-full flex items-center">S'INSCRIRE</Link>
            </div>
            <h1 className='text-2xl font-bold text-primary'>Inscrivez-vous à votre compte pour accéder à l'application.</h1>
            <form className="flex flex-col items-start w-full gap-1">
                <div className="flex">
                    <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                        <label className="font-bold text-primary mt-2">NOM</label>
                        <input value={registerData.name} name='name' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre nom"/>    
                    </div>
                    <div className="flex flex-col gap-1 w-1/2 items-start">
                        <label className="font-bold text-primary mt-2 uppercase">Prénom</label>
                        <input value={registerData.firstName} name='firstName' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre prénom"/>
                    </div>
                </div>
                <label className="font-bold text-primary mt-2">IDENTIFIANT</label>
                <input value={registerData.surname} name='surname' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre identifiant"/>
                <label className="font-bold text-primary mt-2">ADRESSE E-MAIL</label>
                <input value={registerData.email} name='email' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre adresse e-mail"/>
                <label className="font-bold text-primary mt-2 uppercase">Numéro de téléphone</label>
                <input value={registerData.telNumber} name='telNumber' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre numéro de téléphone"/>
                <label className="font-bold text-primary mt-2">MOT DE PASSE</label>
                <input value={registerData.password} name='password' onChange={handleInputChange} type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre mot de passe"/>
                <button onClick={register} className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-4'>S'INSCRIRE</button>
                {/* <Link to='/login' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-4'>S'INSCRIRE</Link> */}
            </form>
            <p className="text-primary text-xs font-semibold">Vous avez déjà un compte ?</p>
            <Link to='/login' className='btn bg-primary text-white font-bold rounded-xl px-12 py-2'>SE CONNECTER</Link>
        </div>
    );
}

export default Register;