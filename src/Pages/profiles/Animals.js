import { Link, useNavigate } from "react-router-dom";
import { motion } from 'framer-motion';
import axios from "axios";
import { useAccountStore } from "../../store";
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import imageChienChat from '../../assets/img/cohabitation-chien-chat.jpg';

const Animals = () => {
    const navigate = useNavigate();
    const { account } = useAccountStore();
    const [animals, setAnimals] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3001/api/animals/user/' + account.user._id)
        .then((res) => {
            setAnimals(res.data);
        })
        .catch((err) => {
            console.log(err);
        });
    }, []);

    const showAnimal = (e) => {
        navigate('/animal/' + e.target.dataset.id);
    };

    return (
        <motion.div 
            className="animals w-full h-full absolute top-0 left-0 p-6"
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <h1 className='text-4xl font-bold text-primary'>Vos animaux</h1>
            <p className="text-primary text-lg font-semibold text-left">Ajoutez vos animaux pour pouvoir les gérer en cas de perte et permettre à d'autres personnes de vous les signaler.</p>
            <div className="w-full max-w-md py-2 px-4 bg-white rounded-lg sm:p-5">
                <div className="flow-root">
                    <ul role="list" className="divide-y divide-gray-200">
                        {
                            animals.map((animal, index) => {
                                return (
                                    <li onClick={showAnimal} key={animal._id} data-id={animal._id} className={"py-3 cursor-pointer " + (index === 0 ? "pt-0" : '') + (index === animals.length - 1 ? 'pb-0' : '')}>
                                        <div className="flex items-center pointer-events-none">
                                            <div className="flex-shrink-0">
                                                <img className="w-8 h-8 rounded-full" src={imageChienChat} alt="Neil animal"/>
                                            </div>
                                            <div className="flex-1 min-w-0 ms-4">
                                                <p className="text-sm font-medium text-gray-900 truncate">
                                                    Espèce
                                                </p>
                                                <p className="text-sm text-gray-500 truncate">
                                                    { animal.species }
                                                </p>
                                            </div>
                                            <div className="w-px h-8 bg-gray-900 rounded-xl"></div>
                                            <div className="flex-1 min-w-0 ms-4">
                                                <p className="text-sm font-medium text-gray-900 truncate">
                                                    Race
                                                </p>
                                                <p className="text-sm text-gray-500 truncate">
                                                    { animal.breed }
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                )
                            })
                        }
                        {
                            animals.length === 0 ? <p className='text-primary text-sm font-semibold w-full'>Vous n'avez pas encore d'animaux</p> : null
                        }
                    </ul>
                </div>
            </div>            
            <Link to='/animaladd-1' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full'>AJOUTER UN ANIMAL</Link>
        </motion.div>
    );
}

export default Animals;