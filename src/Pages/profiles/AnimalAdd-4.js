import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from "react";
import { useCookies } from 'react-cookie';
import axios from 'axios';

const AnimalAdd4 = () => {
    const [cookies] = useCookies(['newAnimal']);

    useEffect(() => {
        axios.post('http://localhost:3001/api/animals', cookies.newAnimal)
        .then((res) => {
            console.log(res);
        });
    }, []);
    
    return (
        <div className="animals w-full h-full">
            <div className="logo w-24 h-24 rounded-full bg-primary"></div>
            <h1 className='text-2xl font-bold text-primary'>Nouvel animal</h1>
            <div className="flex w-full">
                <div className="bg-primary h-[3px] w-full"></div>
            </div>
            <div className="flex flex-col items-center w-full gap-1">
                <div className="flex justify-center items-center w-24 h-24 rounded-full border-[1px] border-secondary mt-6">
                    <FontAwesomeIcon icon="fa-solid fa-check" className="text-secondary text-5xl" />
                </div>
                <h1 className="text-4xl font-bold text-primary mt-4">Nous avons ajouté l'animal !</h1>
                <p className="font-semibold text-primary text-xs mt-2">Profitez bien de vos animaux</p>
                <Link to='/' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-12 uppercase'>Aller à l'accueil</Link>
                <Link to='/animaladd-1' className='btn bg-primary text-white font-bold rounded-xl px-4 py-2.5 w-full mt-2 uppercase'>Ajouter un autre animal</Link>
            </div>
        </div>
    );
}

export default AnimalAdd4;