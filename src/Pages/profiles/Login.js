import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";

import logoGoogle from '../../assets/img/logo-google.png';
import logoFacebook from '../../assets/img/logo-facebook.png';
import logoApple from '../../assets/img/logo-apple.png';


const Login = () => {
    const navigate = useNavigate();
    const { login } = useAuth();
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = new FormData(e.currentTarget);
        const result = await login(
            data.get('email').toString(),
            data.get('password').toString()
        );
        if (result) {
            navigate('/');
        }
    };

    return (
        <div className="login w-full h-full !items-center">
            <div className="bg-primary-light rounded-xl h-[34px] flex">
                <Link to='/login' className="btn bg-primary text-white text-md font-semibold rounded-xl px-4 h-full flex items-center">SE CONNECTER</Link>
                <Link to='/register' className="btn text-white text-md font-semibold px-4 h-full flex items-center">S'INSCRIRE</Link>
            </div>
            <h1 className='text-2xl font-bold text-primary'>Connectez-vous à votre compte pour accéder à l'application.</h1>
            <form className="flex flex-col items-start w-full gap-2" onSubmit={handleSubmit}>
                <label className="font-bold text-primary">EMAIL</label>
                <input type="text" name="email" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre email"/>
                <label className="font-bold text-primary">MOT DE PASSE</label>
                <input type="password" name="password" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Votre mot de passe"/>
                <input type="submit" className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 w-full' value="SE CONNECTER"/>
            </form>
            <div className="flex justify-between items-center w-full">
                <div className="bg-primary h-[1px] w-1/4"></div>
                <p className="text-primary text-xs w-1/3">Ou se connecter avec</p>
                <div className="bg-primary h-[1px] w-1/4"></div>
            </div>
            <div className="flex justify-start items-center w-full px-6 py-2.5 rounded-lg border-[1px] border-primary font-black text-primary">
                <img src={logoGoogle} alt="logo-google" className="w-6 h-6 mr-4"/>
                CONNEXION AVEC GOOGLE
            </div>
            <div className="flex justify-start items-center w-full px-6 py-2.5 rounded-lg border-[1px] border-primary font-black text-primary">
                <img src={logoFacebook} alt="logo-google" className="w-6 h-6 mr-4"/>
                CONNEXION AVEC FACEBOOK
            </div>
            <div className="flex justify-start items-center w-full px-6 py-2.5 rounded-lg border-[1px] border-primary font-black text-primary">
                <img src={logoApple} alt="logo-google" className="w-6 h-6 mr-4"/>
                CONNEXION AVEC APPLE
            </div>
            <div className="bg-primary h-[1px] w-full"></div>
            <p className="text-primary text-xs font-semibold">En vous connectant à votre compte client, Google, Facebook ou Apple, vous acceptez les <span className="underline">conditions générales</span> et la <span className="underline">politique de confidentialité</span>.</p>
        </div>
    );
}

export default Login;