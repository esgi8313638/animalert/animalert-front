import { useEffect } from "react";
import { Link } from 'react-router-dom';

import imageChienChat from '../../assets/img/cohabitation-chien-chat.jpg';

const Welcome = ({ setWelcome }) => {

    useEffect(() => {
        setWelcome(false);
    }, [setWelcome]);

    return (
        <div className="welcome w-full h-full">
            <div className="logo w-32 h-32 rounded-full bg-primary"></div>
            <div className="separator w-11/12 h-0.5 bg-primary"></div>
            <h1 className='text-3xl font-semibold text-primary text-left'>Gérer vos animaux et garder de la proximité avec eux.</h1>
            <p className='text-primary text-left leading-4'>Gérer vos animaux et garder de la proximité avec eux. Identifiez les sur le site afin de pouvoir les identifier auprès d'autres personnes en cas de pertes.</p>
            <img src={imageChienChat} alt='cohabitation-chien-chat' className='w-full rounded-3xl' />
            <Link to='/login' className='btn bg-secondary text-white rounded-2xl px-4 py-2.5 mt-4 w-11/12'>Continuer</Link>
        </div>
    );
}

export default Welcome;