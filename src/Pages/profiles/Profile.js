import { Link } from "react-router-dom";
import { useAccountStore } from "../../store";
import { motion } from 'framer-motion';

const Profile = () => {

    const logout = (e) => {
        e.preventDefault();
        localStorage.removeItem('account');
        window.location.reload();
    };
    const { account } = useAccountStore();

    return (
        <motion.div 
            className="profile w-full h-full absolute top-0 left-0 p-6"
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <h1 className='text-4xl font-bold text-primary'>Votre profil</h1>
            <p className="text-primary text-lg font-semibold text-left">Ajouter et modifier les informations de votre profil.</p>
            <div className="flex flex-col gap-1 items-start">
                <label className="font-bold text-primary">NOM</label>
                <input 
                    type="text" 
                    className="font-semibold border border-secondary rounded-lg p-2 text-primary text-lg leading-5 bg-transparent"
                    value={ account.user.name }
                ></input>

                <label className="font-bold text-primary uppercase">Prénom</label>
                <input 
                    type="text" 
                    className="font-semibold border border-secondary rounded-lg p-2 text-primary text-lg leading-5 bg-transparent"
                    value={ account.user.firstName }
                ></input>

                <label className="font-bold text-primary">IDENTIFIANT</label>
                <input 
                    type="text" 
                    className="font-semibold border border-secondary rounded-lg p-2 text-primary text-lg leading-5 bg-transparent"
                    value={ account.user.surname }
                ></input>

                <label className="font-bold text-primary">ADRESSE E-MAIL</label>
                <input 
                    type="text" 
                    className="font-semibold border border-secondary rounded-lg p-2 text-primary text-lg leading-5 bg-transparent"
                    value={ account.user.email }
                ></input>

                <label className="font-bold text-primary uppercase">Numéro de téléphone</label>
                <input 
                    type="text" 
                    className="font-semibold border border-secondary rounded-lg p-2 text-primary text-lg leading-5 bg-transparent"
                    value={ account.user.telNumber }
                ></input>
            </div>
            <Link to='/animals' className='btn bg-secondary text-white font-bold rounded-xl px-4 py-2.5 uppercase'>Voir mes animaux</Link>
            <button onClick={logout} className='btn bg-primary text-white font-bold rounded-xl px-4 py-2.5 uppercase'>Se déconnecter</button>
        </motion.div>
    );
}

export default Profile;