import { Link, useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from 'react';
import { useAccountStore } from "../store";
import { socket } from "../socket";
import axios from "axios";
import imageChienChat from '../assets/img/cohabitation-chien-chat.jpg';

const AnswerAdd = () => {
    const { account } = useAccountStore();
    const [locationData, setLocationData] = useState({ latitude: '', longitude: '' });
    const [announceData, setAnnounceData] = useState({ animalId: '', title: '', description: '', date: '', adresse: '', latitude: '', longitude: '', type: '' });
    const [animals, setAnimals] = useState([]);
    const [alert, setAlert] = useState({});
    const params = useParams();
    const navigate = useNavigate();

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            setLocationData({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
        });
    }

    useEffect(() => {
        axios.get('http://localhost:3001/api/animals/user/' + account.user._id)
            .then((res) => {
                setAnimals(res.data);
            })
            .catch((err) => {
                console.log(err);
            });

        axios.get('http://localhost:3001/api/alerts/' + params.id)
            .then((res) => {
                setAlert(res.data);
            })
            .catch((err) => {
                console.log(err);
            });

        setAnnounceData(prevState => ({
            ...prevState,
            latitude: locationData.latitude.toString(),
            longitude: locationData.longitude.toString()
        }));
    }, [locationData.latitude, locationData.longitude]);

    const handleInputChange = (e) => {
        const target = e.target;
        let value = target.value;
        const name = target.name;
        setAnnounceData({ ...announceData, [name]: value })
    };

    const handleAnimal = (e) => {
        e.preventDefault();
        const target = e.target;
        const animals = document.querySelectorAll('.animal');
        animals.forEach(animal => {
            animal.firstElementChild.classList.remove('border-secondary');
        });
        e.target.firstElementChild.classList.add('border-secondary');
        setAnnounceData({ ...announceData, userAnimalId: target.dataset.id });
    }

    const createAnswer = (e) => {
        e.preventDefault();
        announceData.userId = account.user._id;
        announceData.alertId = params.id;
        announceData.animalId = alert.animalId;

        axios.post('http://localhost:3001/api/alerts-answers', announceData)
        .then((res) => {
            if (res.data.message !== undefined) {
                return;
            }
            navigate('/answers');
            // socket.emit('newAnnounce', res.data);
        })
        .catch((err) => {
            console.log(err);
        });
    }

    return (
        <div className="animals w-full h-full">
            <h1 className='text-4xl font-bold text-primary'>Répondre à cette annonce</h1>
            <div className="flex w-full">
                <div className="bg-primary h-[3px] w-full"></div>
            </div>
            <form className="flex flex-col items-start w-full gap-1">
                <label className="font-bold text-primary mt-2 uppercase">Type</label>
                <div className="flex gap-2 w-full">
                    <label
                        className={`w-1/2 py-4 px-5 bg-white border rounded-lg cursor-pointer ${animals.length === 0 ? 'disabled border-gray-500' : 'border-green-500'}`}
                    >
                        <input type="radio" name="type" value="C'est mon animal" onChange={handleInputChange} className="hidden" />
                        <div>
                            <span className="check-icon relative inline-block w-5 h-5 border border-black rounded-full"></span>
                            <div className="pt-2">
                                {
                                    animals.length === 0 ? (
                                        <span className="text-lg text-gray-500">Vous n'avez aucun animal</span>
                                    ) : (
                                        <span className="text-lg text-black">C'est mon animal</span>
                                    )
                                }
                            </div>
                        </div>
                    </label>
                    <label
                        className="w-1/2 py-4 px-5 bg-white border border-green-500 rounded-lg cursor-pointer"
                    >
                        <input type="radio" name="type" value="Je connais cet animal" onChange={handleInputChange} className="hidden" />
                        <div>
                            <span className="check-icon relative inline-block w-5 h-5 border border-black rounded-full"></span>
                            <div className="pt-2">
                                <span className="text-lg text-black">Je connais cet animal</span>
                            </div>
                        </div>
                    </label>
                </div>

                {announceData.type === "C'est mon animal" && (
                    <>
                        <label className="font-bold text-primary mt-2 uppercase">Mes animaux</label>
                        <div className='flex flex-wrap gap-6'>
                            {animals.map((animal, index) => {
                                return (
                                <div key={index} onClick={handleAnimal} data-id={ animal._id } className='flex flex-col items-center animal'>
                                    <img className="w-20 h-20 rounded-full object-cover border-[2px] border-primary pointer-events-none" src={imageChienChat} alt="Neil animal"/>
                                    <p className='text-primary font-semibold text-lg uppercase pointer-events-none'>{ animal.name }</p>
                                </div>
                                )
                            })}
                        </div>
                        <label className="font-bold text-primary mt-2 uppercase">Titre de la réponse</label>
                        <input value={announceData.title} onChange={handleInputChange} name="title" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Titre de la réponse"/>
                        <label className="font-bold text-primary mt-2 uppercase">Date</label>
                        <input value={announceData.date} onChange={handleInputChange} name="date" type="date" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Date"/>
                        <label className="font-bold text-primary mt-2 uppercase">Description</label>
                        <input value={announceData.description} onChange={handleInputChange} name="description" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Description"/>
                    </>
                )}
                
                {announceData.type === "Je connais cet animal" && (
                    <>
                        <label className="font-bold text-primary mt-2 uppercase">Titre de la réponse</label>
                        <input value={announceData.title} onChange={handleInputChange} name="title" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Titre de la réponse"/>
                        <label className="font-bold text-primary mt-2 uppercase">Description</label>
                        <input value={announceData.description} onChange={handleInputChange} name="description" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Description"/>
                        <label className="font-bold text-primary mt-2 uppercase">Date</label>
                        <input value={announceData.date} onChange={handleInputChange} name="date" type="date" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Date"/>
                        <label className="font-bold text-primary mt-2 uppercase">Adresse</label>
                        <input value={announceData.adresse} onChange={handleInputChange} name="adresse" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Adresse"/>
                        <div className="flex w-full">
                            <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                                <label className="font-bold text-primary mt-2 uppercase">Espèce</label>
                                <input value={announceData.species} onChange={handleInputChange} name="species" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Espèce" required/>    
                            </div>
                            <div className="flex flex-col gap-1 w-1/2 items-start">
                                <label className="font-bold text-primary mt-2 uppercase">Race</label>
                                <input value={announceData.breed} onChange={handleInputChange} name="breed" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Race" required/>
                            </div>
                        </div>
                        <div className="flex w-full">
                            <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                                <label className="font-bold text-primary mt-2 uppercase">Couleur</label>
                                <input value={announceData.color} onChange={handleInputChange} name="color" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Couleur" required/>    
                            </div>
                            <div className="flex flex-col gap-1 w-1/2 items-start">
                                <label className="font-bold text-primary mt-2 uppercase">Type de poil</label>
                                <input value={announceData.fur} onChange={handleInputChange} name="fur" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Type de poil" required/>
                            </div>
                        </div>
                        {locationData.latitude && locationData.longitude && 
                            <label className="text-primary mt-4">D'après votre localisation</label>
                        }
                        <div className="flex w-full">
                            <div className="flex flex-col gap-1 w-1/2 mr-2 items-start">
                                <label className="font-bold text-primary uppercase">Latitude</label>
                                <input value={announceData.latitude} onChange={handleInputChange} name="latitude" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Longitude" required/>    
                            </div>
                            <div className="flex flex-col gap-1 w-1/2 items-start">
                                <label className="font-bold text-primary uppercase">Longitude</label>
                                <input value={announceData.longitude} onChange={handleInputChange} name="longitude" type="text" className='input w-full rounded-lg border-[1px] border-primary bg-transparent py-2 px-4 placeholder-primary' placeholder="Latitude" required/>
                            </div>
                        </div>
                    </>
                )}
                
                <input value={account.id} onChange={handleInputChange} name="userId" type="text" className='hidden'/>

                <div className="flex gap-x-4 w-full mt-2">
                    <Link to='/' className='btn bg-primary text-white font-bold rounded-xl px-3 py-3 w-1/4'>ANNULER</Link>
                    <button 
                        onClick={announceData.type !== null && announceData.type !== undefined && announceData.type !== '' ? createAnswer : null} 
                        className={`btn font-bold rounded-xl px-4 py-2.5 uppercase w-3/4 ${announceData.type !== null && announceData.type !== undefined && announceData.type !== '' ? 'bg-secondary text-white cursor-pointer' : 'bg-gray-500 text-gray-400 cursor-not-allowed'}`}
                        disabled={announceData.type !== null && announceData.type !== undefined && announceData.type !== '' ? false : true}
                    >
                            Créer
                    </button>
                </div>
            </form>
            
        </div>
    );
}

export default AnswerAdd;