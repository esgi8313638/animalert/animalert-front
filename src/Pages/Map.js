import { motion } from 'framer-motion';
import MapComponent from '../components/Map';
import axios from "axios";
import { useEffect, useState } from "react";

const Map = () => {
    const [announces, setAnnounces] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3001/api/alerts')
            .then((res) => {
                setAnnounces(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <motion.div 
            className='map !items-center w-full h-full absolute top-0 left-0'
            initial={{ x: 0, zIndex: 0 }}
            animate={{ x: 0, zIndex: 0, transition: { duration: 0.4 } }}
            exit={{ x: -window.innerWidth, zIndex: 10, transition: { duration: 0.4 } }}
        >
            <MapComponent announces={announces} />
        </motion.div>
    );
}

export default Map;