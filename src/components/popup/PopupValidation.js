import { Dialog, Transition } from '@headlessui/react'

function PopupValidation({ open, onClose, title, description, color }) {
    return (
        <Dialog 
            open={open} 
            onClose={onClose}
            className="relative z-50"
        >
            <div className="fixed inset-0 bg-black/30" aria-hidden="true" />

            <div className="fixed inset-0 flex w-screen items-center justify-center p-4">
                <Dialog.Panel className="mx-auto p-10 rounded-xl bg-white">
                    <Dialog.Title 
                        className={`text-4xl font-bold ${color ? `text-${color}` : ''}`}
                    >
                        { title }
                    </Dialog.Title>
                    
                    <Dialog.Description className='pt-4'>
                        { description }
                    </Dialog.Description>

                    <button 
                        onClick={onClose}
                        className='btn bg-secondary text-white font-bold rounded-xl px-8 py-2 mt-4 cursor-pointer uppercase' 
                    >
                        Fermer la fenêtre
                    </button>
                </Dialog.Panel>
            </div>
        </Dialog>
    );
}

export default PopupValidation;