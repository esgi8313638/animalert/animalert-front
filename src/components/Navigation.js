import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Navigation.css';

const Navigation = () => {
    const location = useLocation();
    if (location.pathname !== '/login' && location.pathname !== '/register' && location.pathname !== '/welcome' && location.pathname !== '/animaladd-1' && location.pathname !== '/animaladd-2' && location.pathname !== '/animaladd-3' && location.pathname !== '/animaladd-4') {
        return (
            <>
                <ul className="nav bg-white rounded-full flex items-center justify-center align-middle gap-7 w-fit py-1 px-4 absolute bottom-8 z-20">
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/' ? 'active' : '')} to="/">
                            <FontAwesomeIcon icon="fa-house" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/map' ? 'active' : '')} to="/map">
                            <FontAwesomeIcon icon="fa-solid fa-map-location-dot" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/alerts' ? 'active' : '')} to="/alerts">
                            <FontAwesomeIcon icon="fa-solid fa-bell" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                    <li>
                        <Link 
                            className='rounded-full transition w-16 h-16 flex justify-center items-center bg-secondary' 
                            to="/announceadd"
                        >
                            <FontAwesomeIcon 
                                icon="fa-solid fa-plus" 
                                className="text-white text-2xl"
                            />
                        </Link>
                    </li>
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/answers' ? 'active' : '')} to="/answers">
                            <FontAwesomeIcon icon="fa-solid fa-message" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/profile' ? 'active' : '')} to="/profile">
                            <FontAwesomeIcon icon="fa-solid fa-user" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                    <li>
                        <Link className={'rounded-full transition w-12 h-12 flex justify-center items-center ' + (location.pathname === '/animals' ? 'active' : '')} to="/animals">
                            <FontAwesomeIcon icon="fa-solid fa-paw" className="text-primary text-2xl"/>
                        </Link>
                    </li>
                </ul>
            </>
        );
    }
}

export default Navigation;