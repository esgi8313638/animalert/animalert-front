import L from 'leaflet';

const iconAnimal = new L.Icon({
    iconUrl: require("../assets/img/icon-animal.png"),
    iconRetinaUrl: require("../assets/img/icon-animal.png"),
    iconSize: new L.Point(40, 40),
    className: "leaflet-div-icon"
});

export { iconAnimal };