import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import { useRef } from "react";
import { iconAnimal } from '../components/Icon';
import SearchFilters from '../components/SearchFilters';
import { Link, useLocation } from 'react-router-dom';
import "leaflet/dist/leaflet.css";

const MapComponent = (props) => {
    const mapRef = useRef(null);
    const latitude = 45.1875602;
    const longitude = 5.7357819;

    const location = useLocation();

    return (
        <>
            {location.pathname === '/map' && (
                <SearchFilters />
            )}
            <MapContainer 
                center={
                    Array.isArray(props.announces) ? 
                    [latitude, longitude] 
                    : 
                    [props.announces.latitude, props.announces.longitude]
                } 
                zoomControl={false}
                zoom={12} 
                ref={mapRef} 
                style={{height: "100%", width: "100%", zIndex: "10", position: 'absolute', top: '0'}}
            >
                <TileLayer
                    url="https://{s}.tile.jawg.io/jawg-streets/{z}/{x}/{y}{r}.png?access-token=ArBMczsrpTSUySxlC6jeFEa60q6qksy6kwILyOfcefUgwGuC0me6wMtMmao0rNVo"
                />
                {
                    Array.isArray(props.announces) ? (
                        props.announces.map((announce, index) => {
                            return (
                                <Marker key={index} position={[announce.latitude, announce.longitude]} icon={ iconAnimal }>
                                    <Popup>
                                        <span className='font-bold text-primary mt-2'>
                                            { announce.title }
                                        </span>
                                        <Link 
                                            to={`/alert/${announce._id}`}
                                            className='btn block bg-secondary !text-white font-bold rounded-xl px-4 py-2 mt-2 uppercase'
                                        >
                                            Voir l'annonce
                                        </Link>
                                    </Popup>
                                </Marker>
                            )
                        })
                    ) : (
                        <Marker position={[props.announces.latitude, props.announces.longitude]} icon={ iconAnimal }>
                            <Popup>
                                { props.announces.title }
                            </Popup>
                        </Marker>
                    )
                }
            </MapContainer>
        </>
    );
}

export default MapComponent;