import { useEffect, useState } from 'react';
import { Route, Routes, Navigate, useLocation } from 'react-router-dom';
import { useAuth } from '../hooks/useAuth';

import Home from '../Pages/Home';
import Welcome from "../Pages/profiles/Welcome";
import Login from '../Pages/profiles/Login';
import Register from '../Pages/profiles/Register';
import Profile from '../Pages/profiles/Profile';
import Animals from '../Pages/profiles/Animals';
import AnimalAdd1 from '../Pages/profiles/AnimalAdd-1';
import AnimalAdd2 from "../Pages/profiles/AnimalAdd-2";
import AnimalAdd3 from "../Pages/profiles/AnimalAdd-3";
import AnimalAdd4 from "../Pages/profiles/AnimalAdd-4";
import Animal from '../Pages/profiles/Animal';
import Alert from '../Pages/Alert';
import Alerts from '../Pages/Alerts';
import Answers from '../Pages/Answers';
import Answer from '../Pages/Answer';
import AnswerAdd from '../Pages/AnswerAdd';
import AnnounceAdd from '../Pages/AnnounceAdd';
import Map from '../Pages/Map';
import NotFound from '../Pages/NotFound';

import { AnimatePresence } from 'framer-motion';

function AnimatedRoutes() {
    const { status } = useAuth();
    const [welcome, setWelcome] = useState(status === 1 ? false : true);
    const location = useLocation();

    useEffect(() => {
        setWelcome(false);
    }, []);

    return (
        <>
            <AnimatePresence>
                <Routes location={location} key={location.pathname}>
                    <Route path='/' element={<Home />} exact />
                    <Route path='/welcome' element={<Welcome setWelcome={setWelcome} />} />
                    <Route path='/login' element={<Login />} />
                    <Route path='/register' element={<Register />} />
                    <Route path='/map' element={<Map />} />
                    <Route path='/profile' element={<Profile />} />
                    <Route path='/animals' element={<Animals />} />
                    <Route path='/animaladd-1' element={<AnimalAdd1 />} />
                    <Route path='/animaladd-2' element={<AnimalAdd2 />} />
                    <Route path='/animaladd-3' element={<AnimalAdd3 />} />
                    <Route path='/animaladd-4' element={<AnimalAdd4 />} />
                    <Route path='/animal/:id' element={<Animal />} />
                    <Route path='/alert/:id' element={<Alert />} />
                    <Route path='/alerts' element={<Alerts />} />
                    <Route path='/answers' element={<Answers />} />
                    <Route path='/answer/:id' element={<Answer />} />
                    <Route path='/answeradd/:id' element={<AnswerAdd />} />
                    <Route path='/announceadd' element={<AnnounceAdd />} />
                    <Route element={<NotFound />} />
                </Routes>
            </AnimatePresence>
            {/* {status !== 1 && (
                <Navigate to='/login' />
            )} */}
            {welcome && (
                <Navigate to='/welcome' />
            )}
        </>
    );
}

export default AnimatedRoutes;