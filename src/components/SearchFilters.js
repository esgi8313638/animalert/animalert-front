import { useRef  } from 'react';
import { useDraggable } from "react-use-draggable-scroll";
import { useAccountStore } from "../store";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SearchFilters = () => {

    const { account } = useAccountStore();

    const selectTag = (e) => {
        const tags = document.querySelectorAll('.tag-list > div');
        tags.forEach(tag => {
            tag.classList.remove('border-secondary');
            tag.classList.add('border-white');
            tag.children[0].classList.remove('text-secondary');
            tag.children[0].classList.add('text-primary');
        });
        e.target.classList.remove('border-white');
        e.target.classList.add('border-secondary');
        e.target.children[0].classList.remove('text-primary');
        e.target.children[0].classList.add('text-secondary');
    }

    return (
        <>  
            <div className='flex flex-col justify-center items-center gap-y-2 w-full'>  
                <div className='flex justify-between items-center gap-3 px-4 mt-6 w-9/12 lg:w-7/12 h-14 bg-white rounded-full z-20'>
                    <p className='pl-4 text-primary text-xl text-left'>Bonjour, <span className='font-semibold'>{ account !== undefined && account !== null && account.user !== undefined && account.user !== null ? account.user.firstName : '' }</span>.</p>
                    <div className='notif relative' data-count="3">
                        <FontAwesomeIcon icon="fa-solid fa-bell" className='text-primary text-2xl'/>
                    </div>
                </div>
                <div className='flex justify-center items-center w-full z-20'>
                    <div style={{width: 'fit-content'}} className='tag-list flex justify-start items-center gap-2'>
                        <div onClick={selectTag} className='flex justify-center border-[2px] border-secondary items-center w-fit px-4 py-2 bg-white rounded-full cursor-pointer select-none'>
                            <span className='pointer-events-none font-semibold text-secondary'>Animaux perdus</span>
                        </div>
                        <div onClick={selectTag} className='flex justify-center border-[2px] border-white items-center w-fit px-4 py-2 bg-white rounded-full cursor-pointer select-none'>
                            <span className='pointer-events-none font-medium text-primary'>Animaux trouvés / aperçus</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SearchFilters;