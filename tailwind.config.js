/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {
        colors: {
            'primary': '#41241C',
            'primary-light': 'rgba(65, 36, 28, 0.5)',
            'secondary': '#E07A06',
            'secondary-light': 'rgba(224, 122, 6, 0.5)',
        }
        },
    },
    plugins: [],
}

